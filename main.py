def hello(name:str, *args):
    names = ', '.join([name] + list(args))
    print(f'Hello {names}!')

if __name__ == '__main__':
    hello('git','conflicts')
